### **Coding for Humanities**
## Expectations from Irene Martin

I've always wanted learn how to code.  
So i've enrolled for the Coding for Humanites Course and these are my expectations:

* learn basics in **XML**, **Python**, and **R**
* practising those languages
* get support if needed
* get inspired to find my own style in programming

I've already got a good overview from *codecademy* <https://www.codecademy.com> and for a better practise i will still use it in future.  